import requests

BASE_URL = "http://localhost:5000/v1"


def create_session():
    session = requests.Session()
    session.auth = (
        "microsoftflow.digitalfootprint",
        "microsoftflow.digitalfootprint"
    )
    return session.get(BASE_URL+"/brands")


def get_call(url):
    session = create_session()
    response = session.get(BASE_URL + url)
    return response.status_code


def post_call(url):
    session = create_session()
    response = session.post(BASE_URL + url)
    return response.status_code
