from flask import Flask, jsonify, request
from flask_api import status
import json
from authenticate import authenticate
from error_handler import error_handler
from update_request_parser import parse_request
import request_parser

app = Flask(__name__)
app.config["DEBUG"] = True

CONSTANT_STRING1 = "global parameter "
CONSTANT_STRING2 = "parameter size "
CONSTANT_STRING3 = "parameter page "


@error_handler
def get_response(filename):
    with open(filename, encoding="utf8") as file:
        response = json.load(file)
    return jsonify(response), status.HTTP_200_OK


@app.route("/v1/assets/<string:type>", methods=["GET"])
@authenticate
@error_handler
def get_assets_by_type(type):
    if type is None:
        return jsonify(
            {
                "error": "asset type required in url"
            }
        ), status.HTTP_400_BAD_REQUEST

    name = request.args.get("name", None)
    if name is None:
        return jsonify(
            {
                "error": "name required in request parameter"
            }
        ), status.HTTP_400_BAD_REQUEST

    response = request_parser.check_type_enum(type)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_404_NOT_FOUND

    global_param = request.args.get("global", None)
    response = request_parser.parse_boolean(global_param)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING1 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    recent = request.args.get("recent", None)
    response = request_parser.parse_boolean(recent)
    if response != "valid":
        return jsonify(
            {
                "error":
                "recent parameter " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    size = request.args.get("size", None)
    response = request_parser.parse_integer(size)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING2 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/AssetType.json")


@app.route("/v1/assets/id/<string:uuid>", methods=["GET"])
@authenticate
@error_handler
def get_asset_by_id(uuid):
    global_param = request.args.get("global", None)
    response = request_parser.parse_boolean(global_param)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING1 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    recent = request.args.get("recent", None)
    response = request_parser.parse_boolean(recent)
    if response != "valid":
        return jsonify(
            {
                "error":
                "recent parameter " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/AssetID.json")


@app.route("/v1/assets/bulk", methods=["POST"])
@authenticate
@error_handler
def post_assets_bulk():
    body = request.get_json()

    response = request_parser.parse_body(body, dict)

    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    if "assets" not in body.keys():
        return jsonify(
            {
                "error": "required property assets is missing from body"
            }
        ), status.HTTP_400_BAD_REQUEST

    response = request_parser.parse_assets(body["assets"])
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/BulkRetrieve.json")


@app.route("/v1/update", methods=["POST"])
@authenticate
@error_handler
@parse_request
def update():
    return get_response("response/AssetUpdate.json")


@app.route("/v1/update/history", methods=["POST"])
@authenticate
@error_handler
@parse_request
def update_history():
    return get_response("response/UpdateHistory.json")


@app.route("/v1/assets/<string:type>/connected", methods=["GET"])
@authenticate
@error_handler
def get_connected_assets_by_type(type):
    if type is None:
        return jsonify(
            {
                "error": "asset type required in url"
            }
        ), status.HTTP_400_BAD_REQUEST

    name = request.args.get("name", None)
    if name is None:
        return jsonify(
            {
                "error": "name required in request parameter"
            }
        ), status.HTTP_400_BAD_REQUEST

    response = request_parser.check_type_enum(type)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_404_NOT_FOUND

    global_param = request.args.get("global", None)
    response = request_parser.parse_boolean(global_param)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING1 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    page = request.args.get("page", None)
    response = request_parser.parse_integer(page)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING3 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    size = request.args.get("size", None)
    response = request_parser.parse_integer(size)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING2 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/ConnectedAssetsByType.json")


@app.route("/v1/search", methods=["GET"])
@authenticate
@error_handler
def get_recent_search():
    saved_search_id = request.args.get("savedSearchID", None)
    saved_search_name = request.args.get("savedSearchName", None)

    if (saved_search_id is not None and saved_search_name is not None) or (
        saved_search_id is None and saved_search_name is None
    ):
        return jsonify(
            {
                "error":
                "Specify exactly one of savedSearchID / savedSearchName"
            }
        ), status.HTTP_400_BAD_REQUEST

    if saved_search_id is not None:
        response = request_parser.parse_integer(saved_search_id)
        if response != "valid":
            return jsonify(
                {
                    "error":
                    "savedSearchID " + response
                }
            )

    global_param = request.args.get("global", None)
    response = request_parser.parse_boolean(global_param)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING1 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    page = request.args.get("page", None)
    response = request_parser.parse_integer(page)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING3 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    size = request.args.get("size", None)
    response = request_parser.parse_integer(size)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING2 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/GetRecentSearch.json")


@app.route("/v1/search", methods=["POST"])
@authenticate
@error_handler
def post_recent_search():
    query = request.get_json()
    response = request_parser.parse_body(query, dict)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    response = request_parser.parse_query(query)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    global_param = request.args.get("global", None)
    response = request_parser.parse_boolean(global_param)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING1 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    page = request.args.get("page", None)
    response = request_parser.parse_integer(page)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING3 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    size = request.args.get("size", None)
    response = request_parser.parse_integer(size)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING2 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/SearchRecent.json")


@app.route("/v1/search/history", methods=["POST"])
@authenticate
@error_handler
def post_recent_search_history():
    query = request.get_json()
    response = request_parser.parse_body(query, dict)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    response = request_parser.parse_query(query)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    global_param = request.args.get("global", None)
    response = request_parser.parse_boolean(global_param)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING1 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    page = request.args.get("page", None)
    response = request_parser.parse_integer(page)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING3 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    size = request.args.get("size", None)
    response = request_parser.parse_integer(size)
    if response != "valid":
        return jsonify(
            {
                "error":
                CONSTANT_STRING2 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    recent = request.args.get("recent", None)
    response = request_parser.parse_boolean(recent)
    if response != "valid":
        return jsonify(
            {
                "error":
                "parameter recent " + response
            }
        ), status.HTTP_400_BAD_REQUEST
    return get_response("response/SearchHistory.json")


@app.route("/v1/assets/add", methods=["POST"])
@authenticate
@error_handler
def assets_add():
    body = request.get_json()
    response = request_parser.parse_body(body, dict)

    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    if "properties" not in body.keys():
        return jsonify(
            {
                "error": "properties parameter required in request body"
            }
        ), status.HTTP_400_BAD_REQUEST

    response = request_parser.parse_properties(body["properties"])
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    if "assets" in body.keys():
        response = request_parser.parse_assets(body["assets"])
        if response != "valid":
            return jsonify(
                {
                    "error": response
                }
            ), status.HTTP_400_BAD_REQUEST

    if "targetAssetTypes" in body.keys():
        response = request_parser.parse_target_assets_types(
            body["targetAssetTypes"]
        )
        if response != "valid":
            return jsonify(
                {
                    "error": response
                }
            ), status.HTTP_400_BAD_REQUEST

    if "confirm" in body.keys():
        response = request_parser.parse_boolean(body["confirm"])
        if response != "valid":
            return jsonify(
                {
                    "error": "parameter confirm must be of type boolean"
                }
            ), status.HTTP_400_BAD_REQUEST

    fail_on_error = request.args.get("failOnError", None)
    response = request_parser.parse_boolean(fail_on_error)
    if response != "valid":
        return jsonify(
            {
                "error":
                "failOnError parameter " + response
            }
        ), status.HTTP_400_BAD_REQUEST
    return get_response("response/AssetsAdd.json")


@app.route("/v1/task/<string:id>", methods=["GET"])
@authenticate
@error_handler
def get_task(id):
    return get_response("response/GetTask.json")


@app.route("/v1/task/<string:id>/cancel", methods=["POST"])
@authenticate
@error_handler
def cancel_task(id):
    return get_response("response/CancelTask.json")


@app.route("/v1/deltas", methods=["GET"])
@authenticate
@error_handler
def get_deltas():
    type_param = request.args.get("type", None)
    if type_param is None:
        return jsonify(
            {
                "error": "type required in request parameter"
            }
        ), status.HTTP_400_BAD_REQUEST

    response = request_parser.check_type_enum(type_param)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    date = request.args.get("date", None)
    response = request_parser.parse_date(date)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    range_param = request.args.get("range", None)
    response = request_parser.parse_check_range(range_param)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    measure = request.args.get("measure", None)
    response = request_parser.parse_measure(measure)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    page = request.args.get("page", None)
    response = request_parser.parse_integer(page)
    if response != "valid":
        return jsonify(
            {
                "error": CONSTANT_STRING3 + response
            }
        ), status.HTTP_400_BAD_REQUEST

    size = request.args.get("size", None)
    response = request_parser.parse_integer(size)
    if response != "valid":
        return jsonify(
            {
                "error": "parameter size " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/Deltas.json")


@app.route("/v1/deltas/summary", methods=["GET"])
@authenticate
@error_handler
def get_deltas_summary():
    date = request.args.get("date", None)
    response = request_parser.parse_date(date)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    range_param = request.args.get("range", None)
    response = request_parser.parse_check_range(range_param)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/DeltasSummary.json")


@app.route("/v1/ape/hit/nop", methods=["GET"])
@authenticate
@error_handler
def get_newly_opened_ports():
    period = request.args.get("period", None)
    response = request_parser.parse_check_period(period)
    if response != "valid":
        return jsonify(
            {
                "error": response
            }
        ), status.HTTP_400_BAD_REQUEST

    ports = request.args.get("ports", None)
    response = request_parser.parse_ports(ports)
    if response != "valid":
        return jsonify(
            {
                "error": "ports " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    excluded_ports = request.args.get("excludedPorts", None)
    response = request_parser.parse_ports(excluded_ports)
    if response != "valid":
        return jsonify(
            {
                "error": "excludedPorts " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    after = request.args.get("after", None)
    response = request_parser.parse_integer(after)
    if response != "valid":
        return jsonify(
            {
                "error": "after " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    stream = request.args.get("stream", None)
    response = request_parser.parse_boolean(stream)
    if response != "valid":
        return jsonify(
            {
                "error": "stream " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    size = request.args.get("size", None)
    response = request_parser.parse_integer(size)
    if response != "valid":
        return jsonify(
            {
                "error": "size " + response
            }
        ), status.HTTP_400_BAD_REQUEST

    return get_response("response/NewlyOpenedPorts.json")


@app.route("/v1/brands", methods=["GET"])
@authenticate
@error_handler
def get_brands():
    return get_response("response/Brands.json")


@app.route("/v1/organizations", methods=["GET"])
@authenticate
@error_handler
def get_organizations():
    return get_response("response/Organizations.json")


@app.route("/v1/tags", methods=["GET"])
@authenticate
@error_handler
def get_tags():
    return get_response("response/Tags.json")


@app.route("/v1/savedsearches", methods=["GET"])
@authenticate
@error_handler
def get_savedsearches():
    return get_response("response/SavedSearches.json")


if __name__ == "__main__":
    app.run()
