from json.decoder import JSONDecodeError
from flask import jsonify
from flask_api import status
import functools


def error_handler(func):
    @functools.wraps(func)
    def inner_function(*args, **kwargs):
        try:
            response = func(*args, **kwargs)
        except FileNotFoundError:
            message = {"error": "response file not found"}
            return jsonify(message), status.HTTP_500_INTERNAL_SERVER_ERROR
        except JSONDecodeError:
            message = {"error": "error in parsing response"}
            return jsonify(message), status.HTTP_500_INTERNAL_SERVER_ERROR
        else:
            return response

    return inner_function
