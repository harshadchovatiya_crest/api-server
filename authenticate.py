from flask import request, jsonify
from configparser import ConfigParser
from flask_api import status
import functools

configure = ConfigParser()
configure.read("credentials.ini")

username = configure['Credentials']['username']
password = configure['Credentials']['password']


def authenticate(func):
    @functools.wraps(func)
    def check_credentials(*args, **kwargs):
        auth = request.authorization
        if auth is None:
            message = {"error": "basic authentication required"}
            return jsonify(message), status.HTTP_401_UNAUTHORIZED
        if auth.username != username or auth.password != password:
            message = {"error": "Invalid username or password"}
            return jsonify(message), status.HTTP_401_UNAUTHORIZED

        return func(*args, **kwargs)

    return check_credentials
