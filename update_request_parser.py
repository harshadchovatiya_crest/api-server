from flask import request, jsonify
from flask_api import status
import functools
import request_parser


def parse_request(func):
    @functools.wraps(func)
    def parse_update_request(*args, **kwargs):
        body = request.get_json()
        response = request_parser.parse_body(body, dict)
        if response != "valid":
            return jsonify(
                {
                    "error": response
                }
            ), status.HTTP_400_BAD_REQUEST

        if "properties" not in body.keys():
            return jsonify(
                {
                    "error": "properties parameter required in request body"
                }
            ), status.HTTP_400_BAD_REQUEST

        response = request_parser.parse_properties(body["properties"])
        if response != "valid":
            return jsonify(
                {
                    "error": response
                }
            ), status.HTTP_400_BAD_REQUEST

        if "assets" in body.keys():
            response = request_parser.parse_assets(body["assets"])
            if response != "valid":
                return jsonify(
                    {
                        "error": response
                    }
                ), status.HTTP_400_BAD_REQUEST

        if "targetAssetTypes" in body.keys():
            response = request_parser.parse_target_assets_types(
                body["targetAssetTypes"]
            )
            if response != "valid":
                return jsonify(
                    {
                        "error": response
                    }
                ), status.HTTP_400_BAD_REQUEST

        if "query" in body.keys():
            response = request_parser.parse_query(body["query"])
            if response != "valid":
                return jsonify(
                    {
                        "error": response
                    }
                ), status.HTTP_400_BAD_REQUEST

        fail_on_error = request.args.get("failOnError", None)
        response = request_parser.parse_boolean(fail_on_error)
        if response != "valid":
            return jsonify(
                {
                    "error":
                    "failOnError parameter " + response
                }
            ), status.HTTP_400_BAD_REQUEST

        return func(*args, **kwargs)

    return parse_update_request
