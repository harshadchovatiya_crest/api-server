from unittest.mock import patch
import api_call

GET_PATCH_FUNCTION = "api_call.get_call"
POST_PATCH_FUNCTION = "api_call.post_call"


def test_get_brands():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/brands")

    assert status_code == 200


def test_get_organizations():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/organizations")

    assert status_code == 200


def test_get_tags():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/tags")

    assert status_code == 200


def test_get_savedsearches():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/savedsearches")

    assert status_code == 200


def test_get_deltas():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/deltas")

    assert status_code == 200


def test_get_deltas_summary():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/deltas/summary")

    assert status_code == 200


def test_newly_opened_ports():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/ape/hit/nop")

    assert status_code == 200


def test_get_recent_search():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/search")

    assert status_code == 200


def test_get__assets_by_type():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/assets/type")

    assert status_code == 200


def test_get_asset_by_id():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/assets/id/uuid")

    assert status_code == 200


def test_get_connected_assets_by_type():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/assets/type/connected")

    assert status_code == 200


def test_get_task():
    with patch(GET_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.get_call("/task/id")

    assert status_code == 200


def test_update():
    with patch(POST_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.post_call("/update")

    assert status_code == 200


def test_cancel_task():
    with patch(POST_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.post_call("/task/id/cancel")

    assert status_code == 200


def test_update_history():
    with patch(POST_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.post_call("/update/history")

    assert status_code == 200


def test_post_asset_bulk():
    with patch(POST_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.post_call("/assets/bulk")

    assert status_code == 200


def test_post_recent_search():
    with patch(POST_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.post_call("/search")

    assert status_code == 200


def test_post_recent_search_history():
    with patch(POST_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.post_call("/search/history")

    assert status_code == 200


def test_assets_add():
    with patch(POST_PATCH_FUNCTION) as mock_get:
        mock_get.return_value = 200
        status_code = api_call.post_call("/assets/add")

    assert status_code == 200
