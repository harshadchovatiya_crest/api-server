import datetime

ASSET_TYPE = [
    "domain",
    "host",
    "ip_address",
    "ip_block",
    "as",
    "page",
    "ssl_cert",
    "name_server",
    "mail_server",
    "contact"
    ]


def parse_body(body, type):
    if body is None:
        return "request body required"

    if not isinstance(body, type):
        return "request body must be of type object"

    return "valid"


def parse_boolean(param):
    if param is not None and param != "" and param not in ["true", "false"]:
        return "must be either true of false"

    return "valid"


def check_type_enum(type):
    if type.lower() not in ASSET_TYPE:
        return "Invalid asset type"

    return "valid"


def parse_integer(param):
    if param is not None and param != "":
        try:
            param = int(param)
        except ValueError:
            return "must be of type integer and can not be null"

        if param < 0:
            return "must be positive or zero"

    return "valid"


def parse_assets(assets):
    if not isinstance(assets, list):
        return "assets must be of type array"

    for asset in assets:
        if not isinstance(asset, dict):
            return "element of assets must be of type object"

        if "name" not in asset.keys():
            return "asset name is missing in body"

        if not isinstance(asset["name"], str) or asset["name"] == "":
            return "asset name must be of type string and it can not be null"

        if "type" not in asset.keys():
            return "asset type is missing in body"

        if not isinstance(asset["type"], str):
            return "asset type must be of type string"

        if asset["type"].lower() not in ASSET_TYPE:
            return "Invalid asset type: {}".format(asset["type"])

    return "valid"


def parse_target_assets_types(target_assets_type):
    if not isinstance(target_assets_type, list):
        return "targetAssetTypes must be of type array"

    for targetassettype in target_assets_type:
        if not isinstance(targetassettype, str):
            return "targetAssetTypes must be an array of string"

        if targetassettype == "":
            return "value of array targetAssetTypes can not be null"

    return "valid"


def parse_query(query):
    if not isinstance(query, dict):
        return "request body must be of type object"

    if "query" in query.keys() and not isinstance(query["query"], str):
        return "query must be of type string"

    if "filters" not in query.keys():
        return "filter of query required"

    if not isinstance(query["filters"], dict):
        return "filters must be of type object"

    filters = query["filters"]
    if "condition" not in filters.keys():
        return "condition required in filters"

    if not isinstance(filters["condition"], str):
        return "condition must be of type string"

    if filters["condition"] == "":
        return "condition can not be null"

    if "value" not in filters.keys():
        return "value required in filters"

    if not isinstance(filters["value"], list):
        return "value must be of type array"

    for value in filters["value"]:
        if not isinstance(value, dict):
            return "value of filters must be an array of object"

    return "valid"


def parse_properties(properties):
    if not isinstance(properties, list):
        return "properties must be of type array"

    for property_info in properties:
        if not isinstance(property_info, dict):
            return "properties must be an array of object"

        if "name" not in property_info.keys():
            return "name required in properties object"

        if "value" not in property_info.keys():
            return "value required in properties object"

        if not isinstance(property_info["value"], str) and not isinstance(
                property_info["value"], list):
            return "properties value must be of type string or array"

        if "action" in property_info.keys() and (
            not isinstance(property_info["action"], str)
        ):
            return "action of properties must be of type string"

    return "valid"


def parse_date(date):
    if date is not None and date != "":
        try:
            datetime.datetime.strptime(date, "%Y-%m-%d")
        except ValueError:
            return "Incorrect date format, should be YYYY-MM-DD"

    return "valid"


def parse_check_range(range_param):
    if range_param is not None and range_param != "":
        try:
            range_param = int(range_param)
        except ValueError:
            return "range must be of type integer and can not be null"
        else:
            valid_range = [1, 7, 30]
            if range_param not in valid_range:
                error = "Invalid value for parameter range"
                error += " possible value: 1, 7, 30"
                return error

    return "valid"


def parse_check_period(period):
    if period is not None and period != "":
        try:
            period = int(period)
        except ValueError:
            return "period must be of type integer and can not be null"
        else:
            valid_range = [7, 14, 30]
            if period not in valid_range:
                error = "Invalid value for parameter period"
                error += " possible value: 7, 14, 30"
                return error

    return "valid"


def parse_measure(measure):
    valid_measure = ["ADDED", "REMOVED"]
    if measure is not None and measure != "" and measure not in valid_measure:
        error = "Invalid value for parameter measure"
        error += " possible value: ADDED, REMOVED"
        return error

    return "valid"


def parse_ports(ports):
    if ports is not None and ports != "":
        try:
            ports = ports.split(",")
            for port in ports:
                port = int(port)
        except ValueError:
            error = "must be a list of type integer"
            error += " (e.g: 40,8080,50)"
            return error

    return "valid"
